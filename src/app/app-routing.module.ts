import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { HomeComponent } from './pages/home/home.component';
import { ContactUsComponent } from './shared/components/contact-us/contact-us.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { SignUpComponent } from './pages/sign-up/sign-up.component';
import { LoginComponent } from './pages/login/login.component';
import { BuyTicketComponent } from './pages/buy-ticket/buy-ticket.component';
import { BuyCardsComponent } from './pages/buy-cards/buy-cards.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { EventHistoryComponent } from './pages/event-history/event-history.component';
import { PurchaseHistoryComponent } from './pages/purchase-history/purchase-history.component';
import { OptionPopupComponent } from './pages/option-popup/option-popup.component';
import { ForgotPasswordComponent } from './pages/forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './pages/change-password/change-password.component';
import { BusinessListComponent } from './pages/business-list/business-list.component';
import { ResturantDetailComponent } from './pages/resturant-detail/resturant-detail.component';
import { SchoolDetailComponent } from './pages/school-detail/school-detail.component';
import { SearchDataComponent } from './pages/search-data/search-data.component';
import { SupportComponent } from './pages/support/support.component';
import { PrivacyTermsComponent } from './pages/privacy-terms/privacy-terms.component';
import { EventDetailComponent } from './pages/event-detail/event-detail.component';
import { AuthGuard } from './guard/auth.guard';
import { ContactUS } from './pages/contactUs/contactUS.component';
import { UpcomingEventsComponent } from './pages/upcoming-events/upcoming-events.component';
import { PastEventsComponent } from './pages/past-events/past-events.component';
import { MyEventsComponent } from './pages/my-events/my-events.component';
import { SchoolEventsComponent } from './pages/school-events/school-events.component';
import { MyCardsComponent } from './pages/my-cards/my-cards.component';
import { NearDealsEventsComponent } from './pages/near-deals-events/near-deals-events.component';
import { NearDealsComponent } from './pages/near-deals/near-deals.component';
import { AuthModalComponent } from './pages/auth-modal/auth-modal.component';
import { FaqComponent } from './pages/faq/faq.component';

const routes: Routes = [
  {
    path: "",
    component: PagesComponent,
    children: [
      { path: "", component: HomeComponent },
      { path: "contact-us", component: ContactUsComponent },
      { path: "contactus", component: ContactUS },
      { path: "home", component: HomeComponent },
      { path: "about-us", component: AboutUsComponent },
      { path: "sign-up", component: SignUpComponent },
      { path: "login", component: LoginComponent },
      { path: "buy-ticket", component: BuyTicketComponent, canActivate: [AuthGuard] },
      { path: "buy-pass", component: BuyCardsComponent, canActivate: [AuthGuard] },
      { path: "profile", component: ProfileComponent, canActivate: [AuthGuard] },
      { path: "explore-events", component: EventHistoryComponent },
      { path: "purchase-history", component: PurchaseHistoryComponent, canActivate: [AuthGuard] },
      { path: "option-popup", component: OptionPopupComponent },
      { path: "auth-modal", component: AuthModalComponent },
      { path: "forgot-password", component: ForgotPasswordComponent },
      { path: "change-password", component: ChangePasswordComponent, canActivate: [AuthGuard] },
      { path: "businesses/:id", component: BusinessListComponent },
      { path: "resturant/:id", component: ResturantDetailComponent },
      { path: "school/:id", component: SchoolDetailComponent },
      { path: "school-event/:id", component: SchoolEventsComponent },
      { path: 'search', component: SearchDataComponent },
      { path: 'support', component: SupportComponent },
      { path: 'privacy-terms', component: PrivacyTermsComponent },
      { path: 'event-detail', component: EventDetailComponent, canActivate: [AuthGuard] },
      { path: 'upcoming-events', component: UpcomingEventsComponent, canActivate: [AuthGuard] },
      { path: 'past-events', component: PastEventsComponent, canActivate: [AuthGuard] },
      { path: 'event-history', component: MyEventsComponent, canActivate: [AuthGuard] },
      { path: 'my-cards', component: MyCardsComponent, canActivate: [AuthGuard] }   ,
      { path: 'near-events', component: NearDealsEventsComponent } ,
      { path: 'near-deals', component: NearDealsComponent }, 
      { path: 'faq', component: FaqComponent } 
     ]
  }

];


@NgModule({
  imports: [RouterModule.forRoot(routes, {
    enableTracing: false,
    useHash: false,
    initialNavigation: 'enabled',
    paramsInheritanceStrategy: 'always'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
