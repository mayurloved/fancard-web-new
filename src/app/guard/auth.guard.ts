import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';
import { SharedService } from '../shared/services/shared/shared.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { StoredConstants } from '../shared/utils/constants';
import { AuthModalComponent } from '../pages/auth-modal/auth-modal.component';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  isloggedin: boolean;
  bsModalRef: BsModalRef;
  constructor(private sharedService: SharedService,
    private modalService: BsModalService,

    ) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean | UrlTree {
    const userData = this.sharedService.getFromLocalStorage(StoredConstants.USER_DATA)
    if (userData) {
      return true;
    }
    else {
      this.openModal()
    }
  }

  openModal() {
    this.bsModalRef = this.modalService.show(
      AuthModalComponent,
      Object.assign({}, { class: 'gray modal-lg mt-15' })
    );
    this.bsModalRef.content.closeBtnName = 'Close';
  }
}
