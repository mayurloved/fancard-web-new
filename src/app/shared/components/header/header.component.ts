import { Component, OnInit , TemplateRef} from '@angular/core';
import { Router } from '@angular/router';
import { SharedService } from '../../services/shared/shared.service';
import { StoredConstants } from '../../utils/constants';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { OptionPopupComponent } from 'src/app/pages/option-popup/option-popup.component';
import { LoginComponent } from 'src/app/pages/login/login.component';
import { SignUpComponent } from 'src/app/pages/sign-up/sign-up.component';


declare var $: any;
declare var jquery: any;
@Component({
	selector: 'app-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
	modalRef: BsModalRef | null;
	modalRef2: BsModalRef;
	loggedIn;
	constructor
		(
			public router: Router,
			public sharedService: SharedService,
			private modalService: BsModalService
		) { 
		this.sharedService.isUserLoggedIn().subscribe(async (data) => {
			this.loggedIn = data;
		});
		}

	ngOnInit() {
		// $('.navbar-nav .nav-link').click(function(){
		// 	$('.navbar-nav .nav-link').removeClass('active');
		// 	$(this).addClass('active');$
		// })
		
		// $('.ev').click((e)=>{
		// 	// e.preventDefault();
		// 	$('html, body').animate({
		// 		scrollTop: $('#dynamictabstrp').offset().top - 136
		// 	  }, 1000);
		// });

		// if(window.location.hash){
		// 	if (window.location.hash == '#event'){
		// 		$('html,body').animate({
		// 			scrollTop: $('#dynamictabstrp').offset().top - 136
		// 		}, 1000, 'swing');

		// 	}
		//   // smooth scroll to the anchor id
		// 	if (window.location.hash == '#deal'){
		// 		$('html,body').animate({
		// 			scrollTop: $('#dynamictabstrp1').offset().top + 250
		// 		}, 1000, 'swing');
		//  	}
			
		//   }

		
		// $('.deal').click(()=>{
		// 	$('html, body').animate({
		// 		scrollTop: $('#dynamictabstrp1').offset().top - 136
		// 	  }, 1000);
		// });
		
	}
	
	

	navigateUser() {
		const isLoggedIn = this.sharedService.getFromLocalStorage(StoredConstants.IS_LOGGEDIN)
		if(isLoggedIn === true) {		
			this.router.navigateByUrl('/profile')
		} else {
			// this.router.navigateByUrl('/login')
			this.openModal();
		}
	}
	openModal() {
		this.modalRef = this.modalService.show(
		  OptionPopupComponent,
		  Object.assign({}, { class: 'gray modal-lg mt-15' })
		);
		// this.bsModalRef = this.modalService.show(RatingModalComponent);
		this.modalRef.content.closeBtnName = 'Close';
	  }

	openLoginModel() {
		this.modalRef = this.modalService.show(
			LoginComponent,
			Object.assign({}, { class: 'gray modal-lg mt-15' })
		);
		this.modalRef.content.closeBtnName = 'Close';
	}

	openSignUpModel() {
		this.modalRef = this.modalService.show(
			SignUpComponent,
			Object.assign({}, { class: 'gray modal-lg mt-15' })
		);
		this.modalRef.content.closeBtnName = 'Close';
	}
}
