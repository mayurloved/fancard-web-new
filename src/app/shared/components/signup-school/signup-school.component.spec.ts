import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignupSchoolComponent } from './signup-school.component';

describe('SignupSchoolComponent', () => {
  let component: SignupSchoolComponent;
  let fixture: ComponentFixture<SignupSchoolComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupSchoolComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignupSchoolComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
