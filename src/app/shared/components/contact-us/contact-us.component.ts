import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { HttpServiceService } from '../../services/http-service.service';
// import { constants } from '../../constants';
import { HttpService } from '../../services/http/http.service';
import { URLConstant } from '../../utils/constants';
import { SharedService } from '../../services/shared/shared.service';
@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  form;
  contactForm: FormGroup;
  contactFormSubmitted: any = false;
  responseText: any;
  constructor(
    public formBuilder: FormBuilder,
    public httpService: HttpService,
    public sharedService: SharedService

  ) { }

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      name: ['', Validators.required],
      phoneNumber: ['', Validators.required],
      emailAddress: ['', [Validators.required, Validators.email]],
      message: ['']
    });
  }
  contactUsSubmit() {
    this.contactFormSubmitted = true;
    if (this.contactForm.valid) {
      let formValues = this.contactForm.value;
      //call api if valid
      var data = new FormData();
      data.append("Name", formValues.name);
      data.append("Email", formValues.emailAddress);
      data.append("PhoneNumber", formValues.phoneNumber);
      data.append("Message", formValues.message);
      this.httpService.postDataObservable(URLConstant.CONTACT_SUPPORT, data).subscribe(detail => {
        let res: any = detail
        if (detail.status) {
          // this.responseText = detail.message;
          this.sharedService.showToaster(detail.message, 'success')
          this.contactForm.reset();
          this.contactFormSubmitted = false;
        }
      })

    } else {
      return false;
    }
  }

}
