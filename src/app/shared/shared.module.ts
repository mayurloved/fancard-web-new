import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FooterComponent } from './components/footer/footer.component';
import { HeaderComponent } from './components/header/header.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { ContactUsComponent } from './components/contact-us/contact-us.component';
import { TimeFormatPipe } from './pipes/time-format.pipe';
import { SignupSchoolComponent } from './components/signup-school/signup-school.component';
import { RatePipe } from './pipes/rate.pipe';


@NgModule({
  imports: [CommonModule, RouterModule, ReactiveFormsModule],
  declarations: [

    HeaderComponent,
    FooterComponent,
    ContactUsComponent,
    SignupSchoolComponent,
    TimeFormatPipe,
    RatePipe
  ],
  exports: [

    HeaderComponent,
    FooterComponent,
    ContactUsComponent,
    SignupSchoolComponent,
    TimeFormatPipe,
    RatePipe
  ],
  // providers: [HttpServiceService]
})
export class SharedModule { }
