import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'rate'
})
export class RatePipe implements PipeTransform {

  transform(value: any): any {
   
    return  Math.round(value * 2) / 2;
  }

}
