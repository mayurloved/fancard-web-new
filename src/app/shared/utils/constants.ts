export class URLConstant {
  //OLD URL
  // public static BASE_URL = "http://fancard.excellentwebworld.in/admin-panel/";

  //LATEST URL
  public static BASE_URL = "https://fancard.app/dev/admin-panel/";
  public static WEB_SERVICE_STUDENT = "Webservicestudent/";
  public static LOGIN = 'login';
  public static REGISTER = 'register';
  public static FORGOT_PASSWORD = 'forgotpassword';
  public static CONTACT_SUPPORT = "contactsupport";
  public static SETTING = "setting";
  public static CHANGE_PASSWORD = 'changepassword';
  public static ADS_BANNER = 'adsbanner';
  public static SEARCH_BUSINESS = 'searchBusinessWebsite';
  public static BUSINESS_DATA = 'businessdata';
  public static SEARCH_BUSINESS_WEBSITE = 'searchbusinesswebsite';
  public static SEARCH = "SearchBusinessWebsite";
  public static HOME = 'home';
  public static ADD_PASS = 'addpass';
  public static CARD_PURCHASE_PAYMENT = 'cardpurchasepayment'
  public static BOOK_EVENT = 'bookevent';
  public static EVENT_LIST = 'eventlist';
  public static EVENT_DETAILS = 'eventdetail';
  public static CATEGORY = 'category';
  public static ALL_BUSINESS_DATA = 'allbusinessdata';
  public static BUSINESS_DETAILS = 'businessdetails';
  public static GET_SCHOOL_DETAILS = 'getschooldetails/';
  public static MAP_VIEW = 'mapviewnew';
  public static ACTION_DIALOG = 'makeactionlog';
  public static DEALS_BANNER = 'dealsbanner';
  public static UPCOMING_EVENTS = 'upcomingevent';
  public static PAST_EVENTS = 'pastevent';
  public static EVENT_HISTORY_DETAILS = 'eventhistorydetail';
  public static PURCHASE_HISTORY = 'purchasehistory';
  public static FAQ = 'faqs'
  public static ATTENDED_EVENT_LIST = 'attendedeventlist';
  public static EVENT_ATTEND = 'eventattend';
  public static LOGOUT = 'logout';
  public static BUSINESS_DETAIL = "BusinessDetails/";
  public static SPONSORED_BUSINESS = "SponsoredBusiness";
  public static SCHOOLLISTFROMLOCATION = "schoollistForWebsite";
  public static SUPPORT = "Support";
  public static SCHOOL_SIGNUP = "SchoolRegister";
  public static SCHOOL_LIST = 'schoollist';
  public static ABOUT_US = "Pages";
  public static FAQS = "faqs";
  public static LOCATION_WISE_EVENTS = "nearesteventsforwebsite";
  public static LOCATION_WISE_DEALS = "nearestoffersforwebsite";

}

export class StoredConstants {
  public static USER_DATA = "user_data";
  public static IS_LOGGEDIN = "isLoggedIn";


}

