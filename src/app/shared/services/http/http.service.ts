import { Injectable } from '@angular/core';
import { URLConstant } from '../../utils/constants';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Observable } from 'rxjs';
import { SharedService } from '../shared/shared.service';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  BaseUrl = URLConstant.BASE_URL + URLConstant.WEB_SERVICE_STUDENT;

  constructor(
    public http: HttpClient,
    private SharedService: SharedService
  ) { }

  getHeader() {
    const httpOptions = {
      headers: new HttpHeaders({
        key: "fancard123*#*"
      })
    };

    return httpOptions;
  }

  getDataObservable(url, options = {}): Observable<any> {
    // this.SharedService.showLoading();
    options = this.getHeader();
    // //console.log(this.getHeader());
    url = this.BaseUrl + url;
    return this.http.get(url, options);
  }
  postDataObservable(url, body, options?): Observable<any> {
    let httpOptions = this.getHeader();
    url = this.BaseUrl + url;
    return this.http.post(url, body, httpOptions);
  }

  putDataObservable(url, body = {}, options = {}): Observable<any> {
    url = this.BaseUrl + url;
    return this.http.put(url, body, options);
  }

  deleteDataObservable(url, options = {}): Observable<any> {
    url = this.BaseUrl + url;
    return this.http.delete(url, options);
  }
  ///Promises
  getDataPromise(url, options = {}): Promise<{}> {
    this.SharedService.showLoading();
    url = this.BaseUrl + url;
    return new Promise(resolve => {
      this.http.get(url, options).subscribe(
        data => {
          resolve(data);
          this.SharedService.hideLoading();
        },
        err => {
          // //console.log(err);
          this.SharedService.hideLoading();
        }
      );
    });
  }

  postDataPromise(url, body, options = {}): Promise<{}> {
    let httpOptions = this.getHeader();
    // //console.log(httpOptions);
    // //console.log(body);
    url = this.BaseUrl + url;
    return new Promise(resolve => {
      this.http.post(url, body, httpOptions).subscribe(
        data => {
          resolve(data);
        },
        err => {
          // //console.log(err);
        }
      );
    });
  }
}
