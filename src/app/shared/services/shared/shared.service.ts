import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { StoredConstants } from '../../utils/constants';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  showFooter: Boolean = true;
  isLoading: boolean = false;
  searchedData: any = [];
  search_title: any;
  categotyId: any;
  isLoggedIn = false;
  isLoginSubject = new BehaviorSubject<boolean>(this.hasUser());
  constructor(private toastr: ToastrService) {
  }

  showLoading() {
    // //console.log("loder start");
    setTimeout(() => {
      this.isLoading = true;
    }, 1);
  }

  hideLoading() {
    // //console.log("loder stop");

    this.isLoading = false;
  }
  public localeStorage(key, data) {
    localStorage[key] = data;
  }
  public getFromLocalStorage(key) {
    let res = key ? JSON.parse(localStorage.getItem(key)) : null;
    return res;
  }
  public clearLocalStorage() {
    localStorage.clear();
  }

  public removeLocalStorage(key) {
    localStorage.removeItem(key)
  }
  //is logged in user

  hasUser() {
    return !!this.getFromLocalStorage(StoredConstants.USER_DATA);
  }

  // toaster
  showToaster(msg, type?) {
    //console.log(type);

    if (type) {
      if (type == "success") {
        //console.log('if succcess toasr');

        this.toastr.success(msg);
      }
      if (type == "error") {
        this.toastr.error(msg);
      }
    } else {
      this.toastr.show(msg);
    }
  }

  //Get current location

  getPosition(): Promise<any> {
    var options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition((resp) => {
        resolve({ lng: resp.coords.longitude, lat: resp.coords.latitude });
      }, (err: any) => {
        reject(err);
      }, options);
    });
  }
  isUserLoggedIn(): Observable<boolean> {
    return this.isLoginSubject.asObservable();
  }

  logout(): void {
    this.localeStorage(StoredConstants.IS_LOGGEDIN, false)
    this.removeLocalStorage(StoredConstants.USER_DATA)
    this.isLoginSubject.next(false);
  }
  
}
