import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { URLConstant } from 'src/app/shared/utils/constants';


@Component({
  selector: 'app-contactus',
  templateUrl: './contactUs.component.html',
  styleUrls: ['./contactUs.component.scss']
})
export class ContactUS implements OnInit {

  form;
  contactForm:FormGroup;
  contactFormSubmitted: any = false;
  responseText:any;
  
  constructor
  (
    public formBuilder: FormBuilder,
    public httpService : HttpService
  ) { }

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      first_name:['',Validators.required],
      last_name:['',Validators.required],
      emailAddress: ['', [Validators.required, Validators.email]],
      message: ['', Validators.required]
    });
  }

  contactUsSubmit(){
    this.contactFormSubmitted = true;
    if(this.contactForm.valid){
      let formValues  = this.contactForm.value;
      //call api if valid
      var data = new FormData();
      data.append("first_name", formValues.first_name);
      data.append("last_name", formValues.last_name);
      data.append("email", formValues.emailAddress);
      data.append("message", formValues.message);
      this.httpService.postDataObservable(URLConstant.CONTACT_SUPPORT, data).subscribe(detail => {
        let res:any = detail
        if (detail.status) {
          // console.log(detail)
          this.responseText = detail.message;
          setTimeout(() => {
            this.responseText = '';
          }, 5000);
          this.contactForm.reset();
          this.contactFormSubmitted = false;
        }
      })

    }else{
      return false;
    }
  }
}
