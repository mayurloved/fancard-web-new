import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactUS } from './contactUS.component';

describe('ContactUS', () => {
  let component: ContactUS;
  let fixture: ComponentFixture<ContactUS>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactUS ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactUS);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
