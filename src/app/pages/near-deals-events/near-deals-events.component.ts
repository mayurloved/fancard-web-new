import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { SharedService } from 'src/app/shared/services/shared/shared.service';
import { URLConstant } from 'src/app/shared/utils/constants';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-near-deals-events',
  templateUrl: './near-deals-events.component.html',
  styleUrls: ['./near-deals-events.component.scss']
})
export class NearDealsEventsComponent implements OnInit {

  allEvents:any = [];
  eventList:any = [];
  allDeals:any = [];
  dealsList:any = [];
  page:any=1;

  longitude:any;
  latitude:any;

  showCategoryMore: boolean;

  constructor(
    public httpService: HttpService,
    public sharedService: SharedService,
    public router : Router
  ) {
    this.sharedService.getPosition().then(pos => {
      this.longitude = pos.lng;
      this.latitude = pos.lat;
      this._getData('');
    }).catch(err => {
      this._getData('');
    });
   }

  ngOnInit(): void {
    this.showCategoryMore =true;
   
  }

  _getData(test){
    var data = new FormData();
    data.append("Latitude", this.latitude ? this.latitude : 0);
    data.append("Longitude", this.longitude ? this.longitude : 0);
      data.append("Page", this.page);
      this.httpService.postDataPromise(URLConstant.LOCATION_WISE_EVENTS, data).then(res => {
        let data: any = res
        if (data.EventsData.length > 0) {
          console.log(data);
          this.allEvents = data.EventsData;
          this.eventList.push.apply(this.eventList,this.allEvents);
        }
        else{
          this.showCategoryMore = false
        }
      }).catch(err=>{
        this.showCategoryMore = false
      })
  }

  categoryViewMore() {
    this.page = this.page +  1;
    console.log(this.page)
    this._getData('viewmore');
	}

  gotoDetailPage(Businessid) {
    this.router.navigateByUrl('/resturant/' + Businessid);
  }

  navToBuyTicket(event) {
    this.router.navigate(['/buy-ticket'], { state: { event: event } });
  }
}
