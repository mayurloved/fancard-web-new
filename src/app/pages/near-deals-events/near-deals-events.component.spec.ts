import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearDealsEventsComponent } from './near-deals-events.component';

describe('NearDealsEventsComponent', () => {
  let component: NearDealsEventsComponent;
  let fixture: ComponentFixture<NearDealsEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NearDealsEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearDealsEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
