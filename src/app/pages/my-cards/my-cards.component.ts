import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { SharedService } from 'src/app/shared/services/shared/shared.service';
import { StoredConstants, URLConstant } from 'src/app/shared/utils/constants';
import { Router } from '@angular/router';
declare var $: any;
declare var jquery: any;



@Component({
  selector: 'app-my-cards',
  templateUrl: './my-cards.component.html',
  styleUrls: ['./my-cards.component.scss']
})
export class MyCardsComponent implements OnInit {

  userProfileData;
  myPass:any=[];
  curve:any = './assets/card_curve.png'
  myTicket:any=[];
  load:any="none"
  constructor(
    private httpService: HttpService,
    private sharedService: SharedService,
    public router: Router ){ 
    this.userProfileData = this.sharedService.getFromLocalStorage(StoredConstants.USER_DATA);
    
  }

  ngOnInit(): void {
    this.getUserCardDetails();
    setTimeout(() => {
      $(document).ready(function(){
        $('.your-class').slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite:false,
          dots :false,
          prevArrow: '<span class="prev"><i class="fa fa-angle-left fa-4x" aria-hidden="true"></i></span>',
          nextArrow: '<span class="next"><i class="fa fa-angle-right fa-4x" aria-hidden="true"></i></span>',
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: false
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
            ,
            {
              breakpoint: 360,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });
        $('.your-class1').slick({
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite:false,
          dots :false,
          prevArrow: '<span class="prev prev-1"><i class="fa fa-angle-left fa-4x" aria-hidden="true"></i></span>',
          nextArrow: '<span class="next next-1"><i class="fa fa-angle-right fa-4x" aria-hidden="true"></i></span>',
          responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: false,
                dots: false
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            },
            {
              breakpoint: 360,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]
        });
      });
      this.load = 'block';
    }, 4000);
   
  }



  getUserCardDetails() {
    var data = new FormData();
    data.append("LoginId", this.userProfileData.Id);
    this.httpService.postDataObservable(URLConstant.HOME, data).subscribe(detail => {
      if (detail.status) {
        console.log(detail);
        // this.purchaseHistoryData = detail.data ? detail.data : []
        // this.purchaseHistoryData.forEach(element => {
        //   element.CardNumber = element.CardNumber.substr(element.CardNumber.length - 4)
        // });
  
        this.myPass = detail.passes;
        this.myTicket = detail.tickets;
      }
    })
  }

  goTo(_route){
    this.router.navigateByUrl('/'+_route);
  }

}
