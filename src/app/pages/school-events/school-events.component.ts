import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from '../../shared/services/http/http.service';
import { SharedService } from '../../shared/services/shared/shared.service';
import { URLConstant } from '../../shared/utils/constants';

@Component({
  selector: 'app-school-events',
  templateUrl: './school-events.component.html',
  styleUrls: ['./school-events.component.scss']
})
export class SchoolEventsComponent implements OnInit {

  
  map: any;
  foodCardArray: any = [];
  rating: any;
  @ViewChild('map') public mapElement: ElementRef;
  school: any = [];
  page = 1;
  eventData: any = [];
  eventList = []
  schoolId: any;
  constructor(private route: ActivatedRoute,
    private httpService: HttpService,
    private sharedService: SharedService,
    public router: Router,
    public activeRoute: ActivatedRoute) {
    this.activeRoute.params.subscribe(routeParams => {
      this.schoolId = routeParams.id

      if (this.schoolId) {
        this.getSchoolDetail(); 
        this.getSchoolEvents();
      
      }
    });
  }

  ngOnInit(): void {
  }

  getSchoolEvents() {
    if (this.schoolId) {
      var data = new FormData();
      data.append("SchoolId", this.schoolId);
      this.httpService.postDataPromise(URLConstant.EVENT_LIST, data).then(res => {
        let schoolData: any = res
        if (schoolData.status) {
          this.eventList = schoolData.data
        }
      })
    }
  }

  getSchoolDetail() {
    this.httpService.getDataObservable(URLConstant.GET_SCHOOL_DETAILS + this.schoolId + '/' + this.page).subscribe(detail => {
      let res: any = detail;
      this.page = this.page + 1;
      if (res.SchoolData) {
        this.school = detail.SchoolData;
        // this.school.push.apply(this.school, detail.SchoolData);
        //console.log(this.school);
      } else {
        this.school = [];
      }
      if (res.OfferData.length > 0) {
        // this.foodCardArray = (detail.OfferData && detail.OfferData.length > 0) ? detail.OfferData : [];
        this.foodCardArray.push.apply(this.foodCardArray, detail.OfferData);

        this.foodCardArray.forEach(element => {
          element.Rating = Math.round(element.Rating * 2) / 2;

        });
      } else {
        
        this.foodCardArray = [];
      }
      if (res.EventData) {
        // this.eventData = detail.EventData;
        this.eventData.push.apply(this.eventData, detail.EventData);
        // //console.log(this.school,this.school.CardImage);
      } else {
        this.eventData = [];
      }
    })

  }

  gotoDetailPage(Businessid) {
    this.router.navigateByUrl('/resturant/' + Businessid);
  }

  navToBuyTicket(event) {
    this.router.navigate(['/buy-ticket'], { state: { event: event } });
  }

  gotoPage(){
    this.router.navigateByUrl('/contact-us');
  }

}
