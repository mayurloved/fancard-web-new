import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { URLConstant, StoredConstants } from 'src/app/shared/utils/constants';
import { SharedService } from 'src/app/shared/services/shared/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-past-events',
  templateUrl: './past-events.component.html',
  styleUrls: ['./past-events.component.scss']
})
export class PastEventsComponent implements OnInit {

  eventData = [];
  userProfileData;

  constructor(
    private httpService: HttpService,
    private sharedService: SharedService,
    public router: Router


  ) {
    this.userProfileData = this.sharedService.getFromLocalStorage(StoredConstants.USER_DATA);

   }

  ngOnInit(): void {
    this.getEventList();
  }

  getEventList() {
    var data = new FormData();
    data.append("UserId", this.userProfileData.Id);
    this.httpService.postDataObservable(URLConstant.PAST_EVENTS, data).subscribe(detail => {
      if (detail.status) {
        this.eventData = detail.eventdata
      }
    })
  }

  navToEventDetail(event) {
    this.router.navigate(['/event-detail'], { state: { event: event } });
  }

}
