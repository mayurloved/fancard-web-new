import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { SharedService } from '../../shared/services/shared/shared.service';
import { StoredConstants, URLConstant } from '../../shared/utils/constants';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from '../../shared/services/http/http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  profileForm: FormGroup;
  profileFormSubmitted = false;
  userProfileData: any;
  imageURL: any;
  constructor(
    public sharedService: SharedService,
    public formBuilder: FormBuilder,
    public httpService: HttpService,
    private cd: ChangeDetectorRef,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.createProfileForm();
    this.getUserProfile()
  }

  getUserProfile() {
    this.userProfileData = this.sharedService.getFromLocalStorage(StoredConstants.USER_DATA);
    this.setProfileValue()
    //console.log(this.userProfileData)
  }

  setProfileValue() {
    this.profileForm.controls.fullName.setValue(this.userProfileData.Name)
    this.profileForm.controls.emailAddress.setValue(this.userProfileData.Email)
    this.profileForm.controls.phoneNumber.setValue(this.userProfileData.Phone)
    this.profileForm.controls.gender.setValue(this.userProfileData.Gender)
    this.profileForm.controls.dob.setValue(this.userProfileData.DOB)
    this.profileForm.controls.studentId.setValue(this.userProfileData.Id)
    this.profileForm.controls.profileImage.setValue(this.userProfileData.Image)
    this.imageURL = this.userProfileData.Image

  }
  createProfileForm() {
    this.profileForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      emailAddress: [{ value: '', disabled: true }, [Validators.required, Validators.email]],
      phoneNumber: ['', Validators.required],
      gender: ['', Validators.required],
      dob: ['', Validators.required],
      studentId: [{ value: '', disabled: true }, Validators.required],
      profileImage: ['', Validators.required],
    });
  }

  updateProfile() {
    this.profileFormSubmitted = true;
    if (this.profileForm.valid) {
      this.profileFormSubmitted = false;
      let formValues = this.profileForm.value;
      var data = new FormData();
      data.append("FullName", formValues.fullName);
      data.append("Gender", formValues.gender);
      data.append("DOB", formValues.dob);
      data.append("Phone", formValues.phoneNumber);
      data.append("profile", formValues.profileImage);
      data.append("StudentId", this.userProfileData.Id);
      //console.log(data);

      this.httpService.postDataObservable(URLConstant.SETTING, data).subscribe(detail => {
        if (detail.status) {
          //console.log(detail)
          this.sharedService.showToaster(detail.message, 'success')
          this.sharedService.localeStorage(StoredConstants.USER_DATA, JSON.stringify(detail.profile))
          // this.profileForm.reset();
          this.profileFormSubmitted = false;
        } else {
          this.sharedService.showToaster(detail.message, 'error')
        }
      })

    } else {
      return false;
    }
  }

  onFileChange(event) {
      const reader = new FileReader();
    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      //console.log(file);
      reader.onload = () => {
        const res = reader.result;
        this.profileForm.controls.profileImage.setValue(file);
        this.imageURL = res;
        // need to run CD since file load runs outside of zone
        this.cd.markForCheck();
      };
    }
  }
  logout() {
    var data = new FormData();
    data.append("UserId", this.userProfileData.Id);
    this.httpService.postDataObservable(URLConstant.LOGOUT, data).subscribe(detail => {
      if(detail.status) {
        this.sharedService.showToaster(detail.message, 'success');
        this.sharedService.logout()
        this.router.navigateByUrl('/home')
      }
    })
  }

  // onFileChange(event) {
  //   // this.uploadStatus = 0;
  //   if (event.target.files.length > 0) {
  //     let file = event.target.files[0];
  //     //console.log(file);

  //     // this.form.get('bannedList').setValue(file);
  //   }
  // }

}
