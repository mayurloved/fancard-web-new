import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { URLConstant } from 'src/app/shared/utils/constants';
import { Router } from '@angular/router';
import { CompleterService, CompleterData } from 'ng2-completer';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';
declare var $: any;
declare var jquery: any;


@Component({
  selector: 'app-event-history',
  templateUrl: './event-history.component.html',
  styleUrls: ['./event-history.component.scss']
})
export class EventHistoryComponent implements OnInit {
  from_date = new FormControl(moment())
  to_date = new FormControl(moment().add(7, 'days'))
  date = new FormControl(moment([2017, 0, 1])); 
  selected_to_date = moment(new Date()).add(7, 'days').format('DD/MM/YYYY');
  selected_from_date = moment(new Date()).format('DD/MM/YYYY');
  public schoolStr: string = '';
  errortext: any = ''
  serializedDate = new FormControl((new Date()).toISOString());
  eventData = [];
  public searchStr: string;
  schoolList: any = [];
  public schoolData: CompleterData;
  schoolID: any;
  dateErrortext;
  constructor(
    private httpService: HttpService,
    public router: Router,
    private completerService: CompleterService,

  ) {

  }

  ngOnInit(): void {
    this.getEventList();
    this.getSchoolList();
  }

  getSchoolList() {
    this.httpService.getDataObservable(URLConstant.SCHOOL_LIST).subscribe(detail => {
      if (detail.status) {
        this.schoolList = detail.SchoolData;
        this.schoolData = this.completerService.local(this.schoolList, 'SchoolName', 'SchoolName');
      }
    })
  }

  getEventList() {
    this.httpService.postDataObservable(URLConstant.EVENT_LIST, {}).subscribe(detail => {
      if (detail.status) {
        this.eventData = detail.data
      }
    })
  }

  _changeCat(e) {
    const toDate = moment(this.to_date.value).format()
    const fromDate = moment(this.from_date.value).format()
    const isAfter = moment(toDate).isAfter(fromDate)
    console.log(isAfter)
    this.dateErrortext = '';
    this.errortext = ''
    if (!isAfter) {
      this.dateErrortext = "Please select valid date";
    } else {
      if (this.schoolStr != '') {
        // $('#filter').modal('hide');
        // $('#filter').modal('toggle');
        $('#filter').modal('hide')
        var data = new FormData();
        data.append("SchoolId", this.schoolID);
        data.append("EventType", e);
        data.append("DateSearchTo", this.selected_to_date.toString());
        data.append("DateSearchFrom", this.selected_from_date.toString());
        this.httpService.postDataPromise(URLConstant.EVENT_LIST, data).then(res => {
          let schoolData: any = res
          this.eventData = []
          if (schoolData.status) {
            this.eventData = schoolData.data
          }
        })
      } else {
        this.errortext = "Please select your school";
      }
    }
  }

  onFromDateChange(e) {
    this.dateErrortext = '';
    this.selected_from_date = moment(e.value).format('DD/MM/YYYY')
    const toDate = moment(this.to_date.value).format()
    const fromDate = moment(this.from_date.value).format()
    const isAfter = moment(toDate).isAfter(fromDate)
    if (!isAfter) {
      this.dateErrortext = "Please select valid date";
    }
  }
  onToDateChange(e) {
    this.dateErrortext = '';
    this.selected_to_date = moment(e.value).format('DD/MM/YYYY')
    const toDate = moment(this.to_date.value).format()
    const fromDate = moment(this.from_date.value).format()
    const isAfter = moment(toDate).isAfter(fromDate)
    if (!isAfter) {
      this.dateErrortext = "Please select valid date";
    }
  }

  onSelectSchool(e) {
    this.errortext = ""
    if (e && e.title && e.originalObject.SId) {
      this.schoolID = e.originalObject.SId;
    }
  }

  navToBuyTicket(event) {
    this.router.navigate(['/buy-ticket'], { state: { event: event } });
  }

  exploreEvents() {
    this.schoolStr = ''
    var data = new FormData();
    data.append("Search", this.searchStr);
    this.httpService.postDataPromise(URLConstant.EVENT_LIST, data).then(res => {
      let schoolData: any = res
      this.eventData = []
      if (schoolData.status) {
        this.eventData = schoolData.data
      }
    })
  }

  clearFilter() {
    // var d = new Date();
    this.from_date = new FormControl(moment())
    this.to_date = new FormControl(moment().add(7, 'days'))
    this.schoolStr = ''
    $('#filter').modal('hide');
    this.getEventList()
  }
}
