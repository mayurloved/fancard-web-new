import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { URLConstant, StoredConstants } from '../../shared/utils/constants';
import { HttpService } from '../../shared/services/http/http.service';
import { SharedService } from '../../shared/services/shared/shared.service';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  changePswdForm: FormGroup;
  changePswdFormSubmitted: any = false;
  userProfileData: any;

  constructor(public httpService: HttpService,
    public formBuilder: FormBuilder,

    public sharedService: SharedService) { }

  ngOnInit(): void {
    this.createForm();
    this.userProfileData = this.sharedService.getFromLocalStorage(StoredConstants.USER_DATA);
  }

  createForm() {
    this.changePswdForm = this.formBuilder.group({
      OldPassword: ['', Validators.required],
      NewPassword: ['', Validators.required],
      ConfirmPassword: ['', Validators.required]
    }, {
      validator: this.ConfirmedValidator('NewPassword', 'ConfirmPassword')
    });
  }

  ConfirmedValidator(controlName, matchingControlName) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlName];
      const matchingControl = formGroup.controls[matchingControlName];
      if (matchingControl.errors && !matchingControl.errors.confirmedValidator) {
        return;
      }
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({ confirmedValidator: true });
      } else {
        matchingControl.setErrors(null);
      }
    }
  }

  submitData() {
    this.changePswdFormSubmitted = true;
    if (this.changePswdForm.valid) {
      this.changePswdFormSubmitted = false;
      let formValues = this.changePswdForm.value;
      var data = new FormData();
      data.append("OldPassword", formValues.OldPassword);
      data.append("NewPassword", formValues.NewPassword);
      data.append("ConfirmPassword", formValues.ConfirmPassword);
      data.append("UserId", this.userProfileData.Id);
      this.httpService.postDataObservable(URLConstant.CHANGE_PASSWORD, data).subscribe(detail => {
        if (detail.status) {
          //console.log(detail)
          this.sharedService.showToaster(detail.message, 'success')
          this.changePswdForm.reset();
          this.changePswdFormSubmitted = false;
        } else {
          this.sharedService.showToaster(detail.message, 'error')
        }
      })

    } else {
      return false;
    }
  }

}
