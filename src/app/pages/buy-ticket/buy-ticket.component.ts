import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { URLConstant, StoredConstants } from 'src/app/shared/utils/constants';
import { SharedService } from 'src/app/shared/services/shared/shared.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators, MaxLengthValidator } from '@angular/forms';

@Component({
  selector: 'app-buy-ticket',
  templateUrl: './buy-ticket.component.html',
  styleUrls: ['./buy-ticket.component.scss']
})
export class BuyTicketComponent implements OnInit {
  eventData;
  userProfileData;
  paymentCardForm: FormGroup;
  paymentCardFormSubmitted = false;
  public customPatterns = { 'x': { pattern: new RegExp('\[0-9\]') } };

  constructor(
    private httpService: HttpService,
    private sharedService: SharedService,
    private activeRoute: ActivatedRoute,
    private router: Router,
    public formBuilder: FormBuilder


  ) {
    if (this.router.getCurrentNavigation()?.extras?.state?.event) {
      this.eventData = this.router.getCurrentNavigation().extras.state.event;
    } else {
      this.router.navigateByUrl('explore-events')
    }
    // this.eventData = { "Id": "1852", "SchoolId": "35", "Name": "Cricket Competition EWW", "Address": "Gandhinagar", "Location": "0", "Date": "Monday,August 31st 05:00 PM", "Time": "05:00 PM", "Description": "Cricket Competition", "event_id": null, "Status": "1", "CreatedDate": "2020-08-18 00:00:00", "Payment": "1", "TicketPrice": "150", "SId": "35", "SchoolName": "Wonderz primary school", "SchoolAddress": "Big Lake High School, 501 Minnesota Ave E, Big Lak...", "SchoolMapUrl": "https://city-institution-amd/weewonderz-pre-primar...", "SchoolImage": "http://fancard.excellentwebworld.in/admin-panel/assets/images/Card/1cbea8884574386d96365632532991f7.png", "Tax": "100", "Team": "brainerd vs annandale" }
    this.userProfileData = this.sharedService.getFromLocalStorage(StoredConstants.USER_DATA);

  }

  ngOnInit(): void {
    this.paymentForm()
    // this.buyTickets()
  }

  paymentForm() {
    this.paymentCardForm = this.formBuilder.group({
      cardNumber: ['', [Validators.required, Validators.pattern('^[ 0-9]*$'), Validators.minLength(17)]],
      cardHolderName: ['', Validators.required],
      // cardHolderEmail: ['', [Validators.required, Validators.email]],
      cardCVV: ['', [Validators.required, Validators.maxLength(4)]],
      cardExp: ['', Validators.required]
    });
  }

  showInterest() {
      var data = new FormData();
      data.append("UserId", this.userProfileData.Id);
      data.append("EventId", this.eventData.Id);
      data.append("SchoolId", this.eventData.SchoolId);
      data.append("Price", '0');
      data.append("Tax", '0');
      data.append("TotalPrice", '0');
      data.append("CardNumber", ' ');
      data.append("CardHolderName", ' ');
      // data.append("CardHolderEmail", formValues.cardHolderEmail);
      data.append("PaymentCardType", ' ');
      data.append("Exp_date", ' ');
      data.append("CVV_no", ' ');


      this.httpService.postDataObservable(URLConstant.BOOK_EVENT, data).subscribe(detail => {
        if (detail.status) {
          //console.log(detail);
          this.sharedService.showToaster(detail.message, 'success')
          this.paymentCardForm.reset();
          this.paymentCardFormSubmitted = false;
        } else {
          this.sharedService.showToaster(detail.message, 'error')
          this.paymentCardFormSubmitted = false;
        }
      })
    }

  buyTickets() {
    this.paymentCardFormSubmitted = true;
    if (this.paymentCardForm.valid) {
      let formValues = this.paymentCardForm.value;
      const totalPrice = +this.eventData.TicketPrice + +this.eventData.Tax;
      var data = new FormData();
      data.append("UserId", this.userProfileData.Id);
      data.append("EventId", this.eventData.Id);
      data.append("SchoolId", this.eventData.SchoolId);
      data.append("Price", this.eventData.TicketPrice);
      data.append("Tax", this.eventData.Tax);
      data.append("TotalPrice", totalPrice.toString());
      data.append("CardNumber", formValues.cardNumber);
      data.append("CardHolderName", formValues.cardHolderName);
      // data.append("CardHolderEmail", formValues.cardHolderEmail);
      data.append("PaymentCardType", 'visa');
      data.append("Exp_date", formValues.cardExp);
      data.append("CVV_no", formValues.cardCVV);
      this.httpService.postDataObservable(URLConstant.BOOK_EVENT, data).subscribe(detail => {
        if (detail.status) {
          //console.log(detail);
          this.sharedService.showToaster(detail.message, 'success')
          this.paymentCardForm.reset();
          this.paymentCardFormSubmitted = false;
        } else {
          this.sharedService.showToaster(detail.message, 'error')
          this.paymentCardFormSubmitted = false;
        }
      })
    }

  }

  creditCardNumberSpacing(value) {
    const { cardNumber } = this.paymentCardForm.controls;
    let trimmedCardNum = cardNumber.value.replace(/\s+/g, '');
    if (trimmedCardNum.length > 16) {
      trimmedCardNum = trimmedCardNum.substr(0, 16);
    }
    const partitions = trimmedCardNum.startsWith('34') || trimmedCardNum.startsWith('37')
      ? [4, 6, 5]
      : [4, 4, 4, 4];
    const numbers = [];
    let position = 0;
    partitions.forEach(partition => {
      const part = trimmedCardNum.substr(position, partition);
      if (part) numbers.push(part);
      position += partition;
    })
    cardNumber.setValue(numbers.join(' '));
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

}
