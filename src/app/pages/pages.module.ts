import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { PagesComponent } from './pages.component';
import { SharedModule } from '../shared/shared.module';
import { RouterModule } from '@angular/router';

import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AboutUsComponent } from './about-us/about-us.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { LoginComponent } from './login/login.component';
import { BuyCardsComponent } from './buy-cards/buy-cards.component';
import { BuyTicketComponent } from './buy-ticket/buy-ticket.component';
import { EventHistoryComponent } from './event-history/event-history.component';
import { ProfileComponent } from './profile/profile.component';
import { PurchaseHistoryComponent } from './purchase-history/purchase-history.component';
import { OptionPopupComponent } from './option-popup/option-popup.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { BusinessListComponent } from './business-list/business-list.component';
import { Ng2CompleterModule } from "ng2-completer";
import { NgxSkeletonLoaderModule } from 'ngx-skeleton-loader';
import { SHARE_BUTTONS_CONFIG } from 'ngx-sharebuttons';
import { ResturantDetailComponent } from './resturant-detail/resturant-detail.component';
import { ShareIconsModule } from 'ngx-sharebuttons/icons';
import { ShareButtonModule } from 'ngx-sharebuttons/button';
import { ShareButtonsModule } from 'ngx-sharebuttons/buttons';
import { SchoolDetailComponent } from './school-detail/school-detail.component';
import { SearchDataComponent } from './search-data/search-data.component';
import { SupportComponent } from './support/support.component';
import { PrivacyTermsComponent } from './privacy-terms/privacy-terms.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { BsModalRef , ModalModule} from 'ngx-bootstrap/modal';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { ContactUS } from './contactUs/contactUS.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatNativeDateModule, DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE} from '@angular/material/core';
import { MatMomentDateModule } from "@angular/material-moment-adapter";
import { UpcomingEventsComponent } from './upcoming-events/upcoming-events.component';
import { PastEventsComponent } from './past-events/past-events.component';
import { MyEventsComponent } from './my-events/my-events.component';

import { MomentDateModule, MomentDateAdapter } from '@angular/material-moment-adapter';
import { SchoolEventsComponent } from './school-events/school-events.component';
import { MyCardsComponent } from './my-cards/my-cards.component';
import { NearDealsEventsComponent } from './near-deals-events/near-deals-events.component';
import { NearDealsComponent } from './near-deals/near-deals.component';
import { AuthModalComponent } from './auth-modal/auth-modal.component';
import { FaqComponent } from './faq/faq.component';


const DateFormats = {
  parse: {
      dateInput: ['DD/MM/YYYY']
  },
  display: {
      dateInput: 'DD/MM/YYYY',
      monthYearLabel: 'MMM YYYY',
      dateA11yLabel: 'LL',
      monthYearA11yLabel: 'MMMM YYYY',
  },
};


@NgModule({
  imports: [
    CommonModule, 
    SharedModule, 
    RouterModule, 
    ReactiveFormsModule, 
    FormsModule, 
    NgxSkeletonLoaderModule, 
    Ng2CompleterModule, 
    ShareIconsModule, 
    ShareButtonModule, 
    ShareButtonsModule,
    MatSelectModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule, 
    MatInputModule,
    MatMomentDateModule,
    NgxMaskModule.forRoot(),
    ModalModule.forRoot(),
    TabsModule.forRoot(),
  ],
  declarations: [
    HomeComponent,
    PagesComponent,
    AboutUsComponent,
    SignUpComponent,
    LoginComponent,
    BuyCardsComponent,
    BuyTicketComponent,
    EventHistoryComponent,
    ProfileComponent,
    PurchaseHistoryComponent,
    OptionPopupComponent,
    ForgotPasswordComponent,
    ChangePasswordComponent,
    BusinessListComponent,
    ResturantDetailComponent,
    SchoolDetailComponent,
    SearchDataComponent,
    SupportComponent,
    PrivacyTermsComponent,
    EventDetailComponent,
    ContactUS,
    UpcomingEventsComponent,
    PastEventsComponent,
    MyEventsComponent,
    SchoolEventsComponent,
    MyCardsComponent,
    NearDealsEventsComponent,
    NearDealsComponent,
    AuthModalComponent,
    FaqComponent
  ],
  exports: [
    HomeComponent,
    PagesComponent,
    
  ],
  providers: [{
    provide: SHARE_BUTTONS_CONFIG,
    useValue: {
      theme: 'material-dark',
      debug: true
    }
  }, BsModalRef,
  { provide: DateAdapter, useClass: MomentDateAdapter, deps: [MAT_DATE_LOCALE] },
  { provide: MAT_DATE_FORMATS, useValue: DateFormats }]
})
export class PagesModule { }
