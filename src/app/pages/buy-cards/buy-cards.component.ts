import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { URLConstant, StoredConstants } from 'src/app/shared/utils/constants';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { SharedService } from 'src/app/shared/services/shared/shared.service';
import { FormBuilder, FormGroup, FormArray, FormControl, Validators } from '@angular/forms';

interface card {
  name: string;
  value: string;
}

@Component({
  selector: 'app-buy-cards',
  templateUrl: './buy-cards.component.html',
  styleUrls: ['./buy-cards.component.scss']
})
export class BuyCardsComponent implements OnInit {

  cards: card[] = [
    { name: 'Family', value: 'familycard' },
    { name: 'Adult', value: 'adultcard' },
    { name: 'Student', value: 'studentcard' }
  ];
  adultRate = 0;
  childRate = 0;
  schoolList: any = [];
  selectedVal: any = 'familycard'
  selectedSchl: any;

  public addAdultForm: FormGroup
  public addChildForm: FormGroup
  public adultCount: any = 1;
  public childCount: any = 1;
  showSeniorCardDisplay;
  totalPrice = 0;
  serviceTax = 0;
  paymentCardForm: FormGroup;
  paymentCardFormSubmitted = false;
  userProfileData;
  schoolLogo;
  public customPatterns = { 'x': { pattern: new RegExp('\[0-9\]') } };
  showChildBtn = false
  showAdultBtn = false
  constructor(
    private httpService: HttpService,
    private sharedService: SharedService,
    public formBuilder: FormBuilder



  ) {
    this.userProfileData = this.sharedService.getFromLocalStorage(StoredConstants.USER_DATA);

  }

  ngOnInit(): void {
    this.getSchoolList();
    this.cardChange(this.selectedVal)
    // this.createAdultForm();
    // this.createChildForm();
    this.paymentForm()
  }

  paymentForm() {
    this.paymentCardForm = this.formBuilder.group({
      cardNumber: ['', [Validators.required, Validators.pattern('^[ 0-9]*$'), Validators.minLength(17)]],
      cardHolderName: ['', Validators.required],
      // cardHolderEmail: ['', [Validators.required, Validators.email]],
      cardCVV: ['', [Validators.required, Validators.maxLength(4)]],
      cardExp: ['', Validators.required]
    });
  }

  createAdultForm() {
    this.addAdultForm = this.formBuilder.group({
      itemRows:
        this.formBuilder.array([this.initAdultItemRows()])
    });
  }

  createChildForm() {
    this.addChildForm = this.formBuilder.group({
      itemRows:
        this.formBuilder.array([this.initChildItemRows()])
    });
  }

  initAdultItemRows() {
    return this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      type: ['adult'],
    })
  }

  initChildItemRows() {
    return this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      type: ['child'],
    })
  }
  get formAdultArr() {
    return this.addAdultForm.get('itemRows') as FormArray;
  }

  get formChildArr() {
    return this.addChildForm.get('itemRows') as FormArray;
  }
  addAdult() {
    this.adultCount = +this.adultCount + 1;
    this.totalPassPrice('add', 'adult')
    this.formAdultArr.push(this.initAdultItemRows());
  }


  addChild() {
    this.childCount = +this.childCount + 1;
    this.formChildArr.push(this.initChildItemRows());
    this.totalPassPrice('add', 'child')
  }

  removeAdult() {
    if (this.adultCount > 1) {
      const removeIndex = Number(this.adultCount) - 1
      this.formAdultArr.removeAt(removeIndex);
      this.adultCount = +this.adultCount - 1;
      this.totalPassPrice('remove', 'adult')
    }
  }


  removeChild() {
    if (this.childCount) {
      const removeIndex = Number(this.childCount) - 1
      this.formChildArr.removeAt(removeIndex);
      this.childCount = +this.childCount - 1;
      this.totalPassPrice('remove', 'child')
    }
  }

  totalPassPrice(action, type) {
    if (action === 'add') {
      if (type === 'adult') {
        this.totalPrice = +this.totalPrice + +this.adultRate
      } else if (type === 'child') {
        this.totalPrice = +this.totalPrice + +this.childRate
      }
    } else if (action === 'remove') {
      if (type === 'adult') {
        this.totalPrice = +this.totalPrice - +this.adultRate
      } else if (type === 'child') {
        this.totalPrice = +this.totalPrice - +this.childRate
      }
    }
  }

  buyPasses() {
    this.paymentCardFormSubmitted = true
    if (this.addAdultForm.value.itemRows.length === 0 && this.addChildForm.value.itemRows.length === 0) {
      alert('add atleast one child or adult')
    } else if (this.addAdultForm.valid && this.addChildForm.valid && this.paymentCardForm.valid) {
      const adultForms = this.addAdultForm.value.itemRows;
      const childForms = this.addChildForm.value.itemRows;
      const userData = adultForms.concat(childForms)
      var data = new FormData();
      data.append("SchoolId", this.selectedSchl.SId);
      data.append("CardType", this.selectedVal);
      data.append("LoginId", this.userProfileData.Id);
      data.append("AdultCount", this.adultCount.toString());
      data.append("ChildrenCount", this.childCount.toString());
      data.append("UserData", JSON.stringify(userData));
      this.httpService.postDataObservable(URLConstant.ADD_PASS, data).subscribe(detail => {
        if (detail.status) {
          this.cardPayment(detail.data.PurchaseId)
          // this.addAdultForm.reset()
          // this.addChildForm.reset()
        } else {
          this.sharedService.showToaster(detail.message, 'error')
        }
      })
    }
  }

  getSchoolList() {
    this.httpService.getDataObservable(URLConstant.SCHOOL_LIST).subscribe(detail => {
      if (detail.status) {
        this.schoolList = detail.SchoolData;
        this.selectedSchl = this.schoolList[0];
        this.schoolChange(this.selectedSchl);
        // this.schoolLogo = this.schoolList[0].SchoolImage;
      }
    })
  }

  schoolChange(schoolData) {
    this.selectedSchl = schoolData;
    this.schoolLogo = schoolData.SchoolImage
    this.showSeniorCardDisplay = schoolData.SeniorCardDisplay
    this.adultRate = schoolData.AdultPrice
    this.childRate = schoolData.ChildPrice
    this.serviceTax = schoolData.ServiceTax
    this.totalPrice = +schoolData.ServiceTax;
    if (this.showSeniorCardDisplay !== '1') {
      this.selectedVal = 'familycard'
    }
    this.cardChange(this.selectedVal)
    // if (this.addAdultForm.value.itemRows.length > 0) {
    //   const adultUser = this.addAdultForm.value.itemRows.length
    //   this.totalPrice = +this.totalPrice + +this.adultRate * adultUser
    // }
    // if (this.addChildForm.value.itemRows.length > 0) {
    //   const childUser = this.addChildForm.value.itemRows.length
    //   this.totalPrice = +this.totalPrice + +this.childRate * childUser
    // }
  }

  cardPayment(purchaseId) {
    this.paymentCardFormSubmitted = true;
    let formValues = this.paymentCardForm.value;
    var data = new FormData();
    data.append("PurchaseId", purchaseId);
    data.append("CardNumber", formValues.cardNumber);
    data.append("CardHolderName", formValues.cardHolderName);
    // data.append("CardHolderEmail", formValues.cardHolderEmail);
    data.append("PaymentCardType", 'visa');
    data.append("Exp_date", formValues.cardExp);
    data.append("CVV_no", formValues.cardCVV);

    this.httpService.postDataObservable(URLConstant.CARD_PURCHASE_PAYMENT, data).subscribe(detail => {
      if (detail.status) {
        this.paymentCardForm.reset()
        this.addAdultForm.reset()
        this.addChildForm.reset()
        this.paymentCardFormSubmitted = false;
        this.sharedService.showToaster(detail.message, 'success')
      } else {
        this.sharedService.showToaster(detail.message, 'error')
      }
    })
  }

  creditCardNumberSpacing(value) {
    const { cardNumber } = this.paymentCardForm.controls;
    let trimmedCardNum = cardNumber.value.replace(/\s+/g, '');
    if (trimmedCardNum.length > 16) {
      trimmedCardNum = trimmedCardNum.substr(0, 16);
    }
    const partitions = trimmedCardNum.startsWith('34') || trimmedCardNum.startsWith('37')
      ? [4, 6, 5]
      : [4, 4, 4, 4];
    const numbers = [];
    let position = 0;
    partitions.forEach(partition => {
      const part = trimmedCardNum.substr(position, partition);
      if (part) numbers.push(part);
      position += partition;
    })
    cardNumber.setValue(numbers.join(' '));
  }

  cardChange(e) {
    this.totalPrice = +this.serviceTax
    this.adultCount = 0
    this.childCount = 0
    this.createAdultForm();
    this.createChildForm();
    this.showChildBtn = false
    this.showAdultBtn = false
    this.formChildArr.clear()
    if (e === 'studentcard') {
      this.childCount = 1
      this.totalPrice = +this.totalPrice + +this.childRate * this.childCount
      this.showChildBtn = false;
      this.formAdultArr.clear();
      this.createChildForm();

      //change css dynamacally
      var element = document.getElementById("child");
      element.classList.remove("col-lg-6");
      element.classList.remove("col-md-6");
      element.classList.remove("col-sm-6");
      element.classList.add("justify-content-center");

    } else if (e === 'familycard') {
      this.adultCount = 1
      this.childCount = 1
      this.totalPrice = +this.totalPrice + (+this.adultRate * this.adultCount) + (+this.childRate * this.childCount)
      this.showChildBtn = true;
      this.showAdultBtn = true;
      this.createAdultForm();
      this.createChildForm();
      //change css dynamacally
      var element = document.getElementById("child");
      element.classList.add("col-lg-6");
      element.classList.add("col-md-6");
      element.classList.add("col-sm-6");
      element.classList.remove("justify-content-center");


      var element = document.getElementById("adult");
      element.classList.add("col-lg-6");
      element.classList.add("col-md-6");
      element.classList.add("col-sm-6");
      element.classList.add("justify-content-lg-end");
      element.classList.remove("justify-content-lg-center");

    } else if (e === 'adultcard') {
      this.adultCount = 1
      this.totalPrice = +this.totalPrice + +this.adultRate * this.adultCount
      this.showAdultBtn = false;
      this.formChildArr.clear()
      this.createAdultForm();
      //change css dynamacally
      var element = document.getElementById("adult");
      element.classList.remove("col-lg-6");
      element.classList.remove("col-md-6");
      element.classList.remove("col-sm-6");
      element.classList.remove("justify-content-lg-end");
      element.classList.add("justify-content-lg-center");

    } else if (e === 'seniorcard') {
      this.adultCount = 1
      this.totalPrice = +this.totalPrice + +this.adultRate * this.adultCount
      this.showAdultBtn = false;
      this.formChildArr.clear()
      this.createAdultForm();
      var element = document.getElementById("adult");
      element.classList.remove("col-lg-6");
      element.classList.remove("col-md-6");
      element.classList.remove("col-sm-6");
      element.classList.remove("justify-content-lg-end");
      element.classList.add("justify-content-lg-center");
    }
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
