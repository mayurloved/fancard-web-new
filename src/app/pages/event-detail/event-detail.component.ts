import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { URLConstant } from 'src/app/shared/utils/constants';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.scss']
})
export class EventDetailComponent implements OnInit {

  eventData;
  eventId;
  constructor(
    private httpService: HttpService,
    private activeRoute: ActivatedRoute,
    private router: Router
  ) { 
    if (this.router.getCurrentNavigation()?.extras?.state?.event) {
      this.eventData = this.router.getCurrentNavigation().extras.state.event;
    } else {
      this.router.navigateByUrl('event-history')
    }
    
  }

  ngOnInit(): void {
  }

}
