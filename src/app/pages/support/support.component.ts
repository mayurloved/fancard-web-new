import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from '../../shared/services/http/http.service';
import { URLConstant } from '../../shared/utils/constants';

@Component({
  selector: 'app-support',
  templateUrl: './support.component.html',
  styleUrls: ['./support.component.scss']
})
export class SupportComponent implements OnInit {

  form;
  supportForm:FormGroup = null;
  SupportFormSubmitted: any = false;
  responseText:any;
  constructor
  (
    public formBuilder: FormBuilder,
    public httpService : HttpService
  )
  { }

  ngOnInit() {
    this.supportForm = this.formBuilder.group({
      emailAddress: ["", Validators.compose([Validators.required, Validators.email])],
      message: ["", Validators.required],
      subject: ["", Validators.required]
    });
  }

  get f() { return this.supportForm.controls; }


  submitData()
  {
    // //console.log(this.supportForm);
    this.SupportFormSubmitted = true;
    if(this.supportForm.valid){
      let formValues  = this.supportForm.value;
      //call api if valid
      var data = new FormData();
      data.append("Subject", formValues.subject);
      data.append("Email", formValues.emailAddress);
      data.append("Message", formValues.message);
      this.httpService.postDataPromise(URLConstant.SUPPORT, data).then(detail => {
        // //console.log(detail)
        let res:any = detail;
        if(res.status){
          // //console.log(detail)
          this.responseText = res.message;
          setTimeout(() => {
            this.responseText = '';
          }, 5000);
          this.supportForm.reset();
          this.SupportFormSubmitted = false;
        }else{
          alert('cc')
        }
      })

    }else{
      return false;
    }
  }

}
