import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from '../../shared/services/http/http.service';
import { URLConstant } from '../../shared/utils/constants';
import { SharedService } from '../../shared/services/shared/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPasswordForm: FormGroup;
  forgotPasswordFormSubmitted = false;
  constructor(public formBuilder: FormBuilder,
    public sharedService: SharedService,
    public router: Router,
    public httpService: HttpService) { }

  ngOnInit(): void {
    this.createForgotPswdForm();
  }

  createForgotPswdForm() {
    this.forgotPasswordForm = this.formBuilder.group({
      emailAddress: ['', [Validators.required, Validators.email]]
    });
  }
  resetPassword() {
    this.forgotPasswordFormSubmitted = true;
    if (this.forgotPasswordForm.valid) {
      let formValues = this.forgotPasswordForm.value;
      var data = new FormData();
      data.append("UserEmail", formValues.emailAddress);
      this.httpService.postDataObservable(URLConstant.FORGOT_PASSWORD, data).subscribe(detail => {
        if (detail.status) {
          this.sharedService.showToaster(detail.message, 'success')
          this.forgotPasswordForm.reset();
          this.forgotPasswordFormSubmitted = false;
          this.router.navigateByUrl('/login');
        } else {
          this.sharedService.showToaster(detail.message, 'error')
        }
      })

    } else {
      return false;
    }
  }

}
