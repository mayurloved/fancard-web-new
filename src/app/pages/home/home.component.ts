import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../shared/services/http/http.service';
import { URLConstant } from '../../shared/utils/constants';
import { Router } from '@angular/router';
import { CompleterService, CompleterData } from 'ng2-completer';
import { SharedService } from '../../shared/services/shared/shared.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import _ from 'lodash';
import { max } from 'rxjs/operators';
import { LoginComponent } from '../login/login.component';
import { SignUpComponent } from '../sign-up/sign-up.component';
declare var $: any;
declare var jquery: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  modalRef: BsModalRef | null;
  modalRef2: BsModalRef;
  
  public searchStr: string;
  public dealSearchStr: string;
  foodlist: any = [];
  categoryArray: any = [];
  processingForCategory = false;
  allCategories: any = [];
  errorText: any;
  

  public dataService: CompleterData;
  public schoolData: CompleterData;

  longitude: any;
  latitude: any;
  protected searchData = [];
  allEventList:any=[];
  eventList:any = [];
  SearchSchoolImage:any;
  bannerImage: any = [];

  //pagination vars
  max_records = 10;
  page_number:any = 1
  showCategoryMore: boolean = true;
  callFlag:any;
  loggedIn;

  constructor(
    public httpService: HttpService,
    private sharedService: SharedService,
    private completerService: CompleterService,
    public router: Router,
    private modalService: BsModalService
  ) {
    this.sharedService.isUserLoggedIn().subscribe(async (data) => {
			this.loggedIn = data;
		});
  }

  ngOnInit(): void {
    this.getCategories();
    this.getDealsBanner();
  }

  gotoBusinessList(item) {
    this.router.navigateByUrl('/businesses/' + item.id);
  }

  getCategories() {
    this.httpService.postDataObservable(URLConstant.CATEGORY, {}).subscribe(categories => {

      if (categories && categories.categoryData) {

        this.allCategories = categories.categoryData;
        this.categoryArray = this.allCategories;
        // this.showCategoryMore = this.allCategories.length > 8;

        this.searchData = categories.searches;

        // //console.log(this.searchData)
        this.dataService = this.completerService.local(this.searchData, 'name', 'name');
        const schoolList = _.filter(this.searchData, { type: 'school'});
        this.schoolData = this.completerService.local(schoolList, 'name', 'name');

        this.processingForCategory = false;
      } else {
        this.processingForCategory = false;

      }
    }, err => {

      this.processingForCategory = false;

    }
    )
  }


  getDealsBanner() {
    this.httpService.postDataObservable(URLConstant.DEALS_BANNER, {}).subscribe(banner => {
      let res: any = banner;
      if (res.status) {
        this.bannerImage = res.deals;
      }
    }, err => {
    }
    )
  }

  clearSchoolSelect() {
    this.allEventList = [];
  }

  navToBuyTicket(event) {
    this.router.navigate(['/buy-ticket'], { state: { event: event } });
  }
  // onSelectSchool(e) {
  //   this.sharedService.searchedData = [];
  //   this.allEventList = [];
    
  //   if (e && e.title && e.originalObject.id) {
  //     this.SearchSchoolImage = e.originalObject.schoolImage;
  //     this.sharedService.search_title = e.title;
  //     this.sharedService.categotyId = e.originalObject.categoryId;
  //     var data = new FormData();
  //     data.append("SchoolId", e.originalObject.id);
  //     this.httpService.postDataPromise(URLConstant.EVENT_LIST, data).then(detail => {
  //       let res: any = detail;
  //       if (res.status == true) {
  //         this.allEventList = res.data;
  //         console.log(this.allEventList.length)
  //         if(this.allEventList.length < 10){
  //           this.showCategoryMore = false;
  //         }
  //         else{
  //           this.showCategoryMore = true;
  //         }
  //         this.eventList = this.allEventList.length > 9 ? this.allEventList.slice(0, this.max_records) : this.allEventList;
  //         this.callFlag = true;
  //         // console.log(this.callFlag)
  //       } else {
  //         this.errorText = 'error';
  //         this.callFlag = false;
  //         // console.log(this.callFlag)
  //         setTimeout(() => {
  //           this.errorText = '';
  //         }, 1000);
  //         this.sharedService.searchedData = [];
  //       }
  //     })
  //   } else {
  //     //console.log('else');
  //     this.callFlag = true;
  //     // console.log(this.callFlag)
  //     this.allEventList = [];
  //     this.eventList = [];
  //     this.SearchSchoolImage = ''
  //     // this.errorText = 'error';
  //     // setTimeout(() => {
  //     //   this.errorText = '';
  //     // }, 3000);
  //     // this.sharedService.searchedData = [];
  //   }
  // }

  // categoryViewMore() {
  //   //Main logic
  //   let temp:any = [];
  //   let end_index = this.max_records + 10
  //   temp = this.allEventList.slice(this.max_records,end_index );
   
  //   this.eventList.push.apply(this.eventList,temp)
  //   console.log(this.max_records,end_index,temp);
  // //   $('html, body').animate({
  // //     scrollTop: $("#grow").offset().top
  // // }, 2000);
  //   if(temp.length == 0){
  //     this.showCategoryMore =  false;
  //   }
  //   this.max_records = this.max_records + 10;
  // }


  onSelectSchool(e) {
    // console.log(e);
    const schoolId = e.originalObject.id;
     this.router.navigateByUrl('/school-event/' + schoolId)
   }
  
  onSelect(e) {
    this.sharedService.searchedData = [];
    this.sharedService.search_title = '';
    this.sharedService.categotyId = '';

    //console.log(e);

    if (e.title && e.originalObject.categoryId) {
      this.sharedService.search_title = e.title;
      this.sharedService.categotyId = e.originalObject.categoryId;
      var data = new FormData();

      data.append("Id", e.originalObject.id);
      data.append("Type", e.originalObject.type);
      data.append("Latitude", this.latitude ? this.latitude : 0.000000);
      data.append("Longitude", this.longitude ? this.longitude : 0.000000);
      data.append("zipcode", '');
      this.httpService.postDataPromise(URLConstant.SEARCH, data).then(detail => {
        let res: any = detail;
        if (res) {
          this.sharedService.searchedData.push(res);

          // //console.log(this.sharedService.searchedData);

          this.router.navigateByUrl('/search');

        } else {
          this.errorText = 'error';
          setTimeout(() => {
            this.errorText = '';
          }, 1000);
          this.sharedService.searchedData = [];
        }
      })
    } else {
      this.sharedService.hideLoading();
      this.errorText = 'error';
      setTimeout(() => {
        this.errorText = '';
      }, 3000);
      this.sharedService.searchedData = [];
    }
  }

  openLoginModel() {
		this.modalRef = this.modalService.show(
			LoginComponent,
			Object.assign({}, { class: 'gray modal-lg mt-15' })
		);
		this.modalRef.content.closeBtnName = 'Close';
	}

	openSignUpModel() {
		this.modalRef = this.modalService.show(
			SignUpComponent,
			Object.assign({}, { class: 'gray modal-lg mt-15' })
		);
		this.modalRef.content.closeBtnName = 'Close';
	}


}
