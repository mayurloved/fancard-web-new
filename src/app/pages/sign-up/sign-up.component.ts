import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { SharedService } from '../../shared/services/shared/shared.service';
import { HttpService } from '../../shared/services/http/http.service';
import { URLConstant, StoredConstants } from '../../shared/utils/constants';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.scss']
})
export class SignUpComponent implements OnInit {
  modalRef: BsModalRef | null;
  modalRef2: BsModalRef;


  form;
  signUpForm: FormGroup;
  signUpFormSubmitted: any = false;
  responseText: any;
  longitude: any;
  latitude: any;
  constructor(
    public formBuilder: FormBuilder,
    public sharedService: SharedService,
    public httpService: HttpService,
    public bsModalRef: BsModalRef,  
    private modalService: BsModalService
  ) {
    this.sharedService.showFooter = false;

  }

  ngOnInit(): void {
    this.createSignUpForm();
    this.getPosition();

  }

  createSignUpForm() {
    this.signUpForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      emailAddress: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required],
      gender: ['', [Validators.required]],
      dob: ['', Validators.required]
    });
  }

  getPosition() {
    this.sharedService
      .getPosition()
      .then((pos) => {
        this.longitude = pos.lng;
        this.latitude = pos.lat;
      })
      .catch((err) => {
      });
  }

  submitData() {
    this.signUpFormSubmitted = true;
    if (this.signUpForm.valid) {
      this.signUpFormSubmitted = false;
      let formValues = this.signUpForm.value;
      var data = new FormData();
      data.append("Name", formValues.fullName);
      data.append("Email", formValues.emailAddress);
      data.append("Password", formValues.password);
      data.append("Gender", formValues.gender);
      data.append("DOB", formValues.dob);
      data.append("Latitude", this.latitude);
      data.append("Longitude", this.longitude);
      this.httpService.postDataObservable(URLConstant.REGISTER, data).subscribe(detail => {
        if (detail.status) {
          //console.log(detail)
          this.responseText = detail.message;
          this.sharedService.showToaster(detail.message, 'success')
          this.sharedService.localeStorage(StoredConstants.USER_DATA, JSON.stringify(detail.profile))
          this.signUpForm.reset();
          this.signUpFormSubmitted = false;
        } else {
          this.sharedService.showToaster(detail.message, 'error')
        }
      })

    } else {
      return false;
    }
  }

}
