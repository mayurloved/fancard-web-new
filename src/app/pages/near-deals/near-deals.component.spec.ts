import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NearDealsComponent } from './near-deals.component';

describe('NearDealsComponent', () => {
  let component: NearDealsComponent;
  let fixture: ComponentFixture<NearDealsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NearDealsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NearDealsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
