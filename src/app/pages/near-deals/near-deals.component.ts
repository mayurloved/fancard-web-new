import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { SharedService } from 'src/app/shared/services/shared/shared.service';
import { Router } from '@angular/router';
import { URLConstant } from 'src/app/shared/utils/constants';

@Component({
  selector: 'app-near-deals',
  templateUrl: './near-deals.component.html',
  styleUrls: ['./near-deals.component.scss']
})
export class NearDealsComponent implements OnInit {

  allEvents:any = [];
  eventList:any = [];
  allDeals:any = [];
  dealsList:any = [];
  page:any=1;

  longitude:any;
  latitude:any;

  showCategoryMore: boolean;

  constructor(
    public httpService: HttpService,
    public sharedService: SharedService,
    public router : Router
  ) {
    this.sharedService.getPosition().then(pos => {
      this.longitude = pos.lng;
      this.latitude = pos.lat;
      this._getData();
    }).catch(err => {
      this._getData();
    });
   }

  ngOnInit(): void {
    this.showCategoryMore =true;
   
  }

  _getData(){
    var data = new FormData();
    data.append("Latitude", this.latitude ? this.latitude : 0);
    data.append("Longitude", this.longitude ? this.longitude : 0 );
      data.append("Page", this.page);
      this.httpService.postDataPromise(URLConstant.LOCATION_WISE_DEALS, data).then(res => {
        let data: any = res
        if (data.OfferData.length > 0) {
            console.log(data);
            this.allDeals = data.OfferData;
            this.dealsList.push.apply(this.dealsList,this.allDeals);
        }else{
          this.showCategoryMore = false;
        }
        
      }).catch(err=>{
        this.showCategoryMore = false
      })
  }

  categoryViewMore() {
    this.page = this.page +  1;
    console.log(this.page)
    this._getData();
	}

  gotoDetailPage(Businessid) {
    this.router.navigateByUrl('/resturant/' + Businessid);
  }

  navToBuyTicket(event) {
    this.router.navigate(['/buy-ticket'], { state: { event: event } });
  }
}

