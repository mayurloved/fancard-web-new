import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { URLConstant, StoredConstants } from 'src/app/shared/utils/constants';
import { SharedService } from 'src/app/shared/services/shared/shared.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-purchase-history',
  templateUrl: './purchase-history.component.html',
  styleUrls: ['./purchase-history.component.scss']
})
export class PurchaseHistoryComponent implements OnInit {

  userProfileData;
  purchaseHistoryData = []
  constructor(
    private httpService: HttpService,
    private sharedService: SharedService,
    public router: Router
  ) { 
    this.userProfileData = this.sharedService.getFromLocalStorage(StoredConstants.USER_DATA);
  }

  ngOnInit(): void {
    this.getPurchaseHistory();
  }

  getPurchaseHistory() {
    var data = new FormData();
    data.append("UserId", this.userProfileData.Id);
    this.httpService.postDataObservable(URLConstant.PURCHASE_HISTORY, data).subscribe(detail => {
      if (detail.status) {
        //console.log(detail);
        this.purchaseHistoryData = detail.data ? detail.data : []
        this.purchaseHistoryData.forEach(element => {
          element.CardNumber = element.CardNumber.substr(element.CardNumber.length - 4)
        });
      }
    })
  }
}
