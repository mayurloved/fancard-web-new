import { Component, OnInit } from '@angular/core';
import { SharedService } from '../shared/services/shared/shared.service';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.scss']
})
export class PagesComponent implements OnInit {

  showFooter: Boolean
  constructor(
    public sharedService: SharedService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.showFooter = this.sharedService.showFooter
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0)
    });
  }

}
