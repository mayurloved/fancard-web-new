import { Component, OnInit } from '@angular/core';
import { SharedService } from '../../shared/services/shared/shared.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HttpService } from '../../shared/services/http/http.service';
import { URLConstant, StoredConstants } from '../../shared/utils/constants';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { SignUpComponent } from '../sign-up/sign-up.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  modalRef: BsModalRef | null;
  modalRef2: BsModalRef;
  

  loginForm:FormGroup;
  loginFormSubmitted = false;
  responseText: any;
  longitude: any;
  latitude: any;
  constructor(public sharedService: SharedService,
    public httpService: HttpService,

    public router: Router,
    public bsModalRef: BsModalRef,  
    private modalService: BsModalService,
    public formBuilder: FormBuilder) {
    this.sharedService.showFooter = false;
  }

  ngOnInit(): void {
    this.createLoginForm();
    this.getPosition();
  }

  getPosition() {
    this.sharedService
      .getPosition()
      .then((pos) => {
        this.longitude = pos.lng;
        this.latitude = pos.lat;
      })
      .catch((err) => {
      });
  }

  createLoginForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  loginUser() {
    this.loginFormSubmitted = true;
    if (this.loginForm.valid) {
      let formValues = this.loginForm.value;
      var data = new FormData();
      data.append("Email", formValues.email);
      data.append("Password", formValues.password);
      data.append("Latitude", this.latitude);
      data.append("Longitude", this.longitude);
      this.httpService.postDataObservable(URLConstant.LOGIN, data).subscribe(detail => {
        if (detail.status) {
          this.sharedService.showToaster(detail.message, 'success')
          // this.router.navigateByUrl('/home')
          this.sharedService.localeStorage(StoredConstants.USER_DATA, JSON.stringify(detail.profile))
          this.sharedService.localeStorage(StoredConstants.IS_LOGGEDIN, true)
          this.sharedService.isLoginSubject.next(true);
          this.loginForm.reset();
          this.loginFormSubmitted = false;
          this.bsModalRef.hide();
        } else {
          this.sharedService.showToaster(detail.message, 'error')
        }
      })

    } else {
      return false;
    }
  }

  navToSignUp() {
    // this.router.navigateByUrl('/sign-up');
    this.modalRef = this.modalService.show(
		  SignUpComponent,
		  Object.assign({}, { class: 'gray modal-lg mt-15' })
		);
		// this.bsModalRef = this.modalService.show(RatingModalComponent);
    this.modalRef.content.closeBtnName = 'Close';
  }

}
