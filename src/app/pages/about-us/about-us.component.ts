import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { URLConstant } from 'src/app/shared/utils/constants';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.component.html',
  styleUrls: ['./about-us.component.scss']
})
export class AboutUsComponent implements OnInit {
  aboutus_desc: any = [];
  quot_text: any;
  ceo_name: any;
  constructor(
    public httpService: HttpService,
    public router: Router
  ) { }

  ngOnInit(): void {
    this.getDescription()
  }

  getDescription() {
    this.httpService.getDataObservable(URLConstant.ABOUT_US)
      .subscribe(desc => {
        let res: any = desc;
        this.aboutus_desc = res;
        let text: String = this.aboutus_desc?.quote_section[0].sectioncontent;
        let result = text.replace('"', '\"');
        let quotes = result.split('---');
        this.quot_text = quotes[0];

        let ceoName = quotes[1].replace('"', '').replace("---", '');
        this.ceo_name = ceoName;
        // console.log('', result);
      }, err => {
        // console.log('err', err);
      })
  }

  gotoContactUs(){
    this.router.navigateByUrl('/contact-us');
  }

}
