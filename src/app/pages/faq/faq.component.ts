import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { URLConstant } from 'src/app/shared/utils/constants';

declare var $: any;
declare var jquery: any;
@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {

  isshow: any = false;
  faqs;
  constructor(
    public httpService: HttpService

  ) { }

  ngOnInit() {
    this.togglebtn();
    this.getFaqData()
  }

  togglebtn() {
    $(".collapse.show").each(function () {
      $(this).siblings(".hrBorder-header").find(".fa").addClass("fa-minus").removeClass("fa-plus");
    });
    $(".collapse").on('show.bs.collapse', function () {
      $(this).parent().find(".fa").removeClass("fa-plus").addClass("fa-minus");
    }).on('hide.bs.collapse', function () {
      $(this).parent().find(".fa").removeClass("fa-minus").addClass("fa-plus");
    });
  }

  getFaqData() {
    this.httpService.postDataObservable(URLConstant.FAQS, {})
      .subscribe(data => {
        let res: any = data;
        if (res.status) {
          this.faqs = res.faqdata
          console.log(this.faqs);
          
        }
      }, err => {
        // console.log('err', err);
      })
  }

}
