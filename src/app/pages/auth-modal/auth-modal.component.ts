import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { CompleterData, CompleterService } from 'ng2-completer';
import { HttpService } from 'src/app/shared/services/http/http.service';
import { SharedService } from 'src/app/shared/services/shared/shared.service';
import { Router } from '@angular/router';
import { URLConstant } from 'src/app/shared/utils/constants';
import { LoginComponent } from '../login/login.component';
import { SignUpComponent } from '../sign-up/sign-up.component';

@Component({
  selector: 'app-auth-modal',
  templateUrl: './auth-modal.component.html',
  styleUrls: ['./auth-modal.component.scss']
})
export class AuthModalComponent implements OnInit {
    modalRef: BsModalRef | null;
    modalRef2: BsModalRef;
    
   
    schoolList: any = [];
    public schoolData: CompleterData;
    searchStr:''
    constructor(
      public sharedService: SharedService,
      private httpService: HttpService,
      private completerService: CompleterService,
      public bsModalRef: BsModalRef,  
      private modalService: BsModalService,
      public router: Router
    ) {
      this.sharedService.showFooter = false;
    
     }
  
    ngOnInit(): void {
      this.getSchoolList();
    }
  
    getSchoolList() {
      this.httpService.getDataObservable(URLConstant.SCHOOL_LIST).subscribe(detail => {
        if (detail.status) {
          //console.log(detail);
          this.schoolList = detail.SchoolData;
          this.schoolData = this.completerService.local(this.schoolList, 'SchoolName', 'SchoolName');
  
        }
      })
    }
  
    navToLogin() {
      // this.router.navigateByUrl('/login')
  
      this.modalRef = this.modalService.show(
        LoginComponent,
        Object.assign({}, { class: 'gray modal-lg mt-15' })
      );
      // this.bsModalRef = this.modalService.show(RatingModalComponent);
      this.modalRef.content.closeBtnName = 'Close';
     
    }
  
    
  
    navToSignUp() {
      // this.router.navigateByUrl('/sign-up')
      this.modalRef = this.modalService.show(
        SignUpComponent,
        Object.assign({}, { class: 'gray modal-lg mt-15' })
      );
      // this.bsModalRef = this.modalService.show(RatingModalComponent);
      this.modalRef.content.closeBtnName = 'Close';
    }
  
    onSelectSchool(e) {
     //console.log(e);
      const schoolId = e.originalObject.SId;
      this.router.navigateByUrl('/school-event/' + schoolId)
    }
  
  }
  
